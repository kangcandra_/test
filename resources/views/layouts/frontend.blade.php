	<!DOCTYPE html>
	<html lang="zxx" class="no-js')}}">
	<head>
		<!-- Mobile Specific Meta -->
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<!-- Favicon-->
		<link rel="shortcut icon" href="{{ asset('frontend/img/ico.png')}}">
		<!-- Author Meta -->
		<meta name="author" content="codepixer">
		<!-- Meta Description -->
		<meta name="description" content="">
		<!-- Meta Keyword -->
		<meta name="keywords" content="">
		<!-- meta character set -->
		<meta charset="UTF-8">
		<!-- Site Title -->
		<title>Assalaam</title>

		<link href="https://fonts.googleapis.com/css?family=Poppins:100,200,400,300,500,600,700" rel="stylesheet"> 
			<!-- CSS ============================================= -->
			<link rel="stylesheet" href="{{ asset('frontend/css/linearicons.css')}}">
			<link rel="stylesheet" href="{{ asset('frontend/css/font-awesome.min.css')}}">
			<link rel="stylesheet" href="{{ asset('frontend/css/bootstrap.css')}}">
			<link rel="stylesheet" href="{{ asset('frontend/css/magnific-popup.css')}}">
			<link rel="stylesheet" href="{{ asset('frontend/css/nice-select.css')}}">	
			<link rel="stylesheet" href="{{ asset('frontend/css/hexagons.min.css')}}">							
			<link rel="stylesheet" href="{{ asset('frontend/css/animate.min.css')}}">
			<link rel="stylesheet" href="{{ asset('frontend/css/owl.carousel.css')}}">
			<link rel="stylesheet" href="{{ asset('frontend/css/main.css')}}">
			<link rel="stylesheet" href="{{ asset('frontend/css/lightbox.min.css')}}">
			{{-- <link rel="stylesheet" href="{{ asset('frontend/css/lightbox.css')}}"> --}}
		</head>
		<body>	
        <!-- #header -->
        @include('part.frontend.head')
        <!-- #end header -->
      
        {{-- konten   --}}
        @yield('content')
        {{--  end konten  --}}

        {{--  footer  --}}
        @include('part.frontend.footer')
        {{--  end footer  --}}
			<script src="{{ asset('frontend/js/vendor/jquery-2.2.4.min.js')}}"></script>
			<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
			<script src="{{ asset('frontend/js/vendor/bootstrap.min.js')}}"></script>			
			<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBhOdIF3Y9382fqJYt5I_sswSrEw5eihAA"></script>
  			<script src="{{ asset('frontend/js/easing.min.js')}}"></script>			
			<script src="{{ asset('frontend/js/hoverIntent.js')}}"></script>
			<script src="{{ asset('frontend/js/superfish.min.js')}}"></script>	
			<script src="{{ asset('frontend/js/jquery.ajaxchimp.min.js')}}"></script>
			<script src="{{ asset('frontend/js/jquery.magnific-popup.min.js')}}"></script>	
			<script src="{{ asset('frontend/js/owl.carousel.min.js')}}"></script>	
			<script src="{{ asset('frontend/js/hexagons.min.js')}}"></script>							
			<script src="{{ asset('frontend/js/jquery.nice-select.min.js')}}"></script>	
			<script src="{{ asset('frontend/js/jquery.counterup.min.js')}}"></script>
			<script src="{{ asset('frontend/js/waypoints.min.js')}}"></script>							
			<script src="{{ asset('frontend/js/mail-script.js')}}"></script>	
			<script src="{{ asset('frontend/js/main.js')}}"></script>
			<script src="{{ asset('frontend/js/lightbox-plus-jquery.min.js')}}"></script>
		</body>
	</html>



