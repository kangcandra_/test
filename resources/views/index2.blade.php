    <div class="tz-gallery">
        <div class="row">
          @foreach($instagram as $key => $image)
            <div class="col-sm-12 col-md-4">
                <a class="lightbox" href="{{ $image->images->standard_resolution->url}}">
                    <img src="{{ $image->images->standard_resolution->url }}" alt="Instagram Feed" class="img-responsive">
                </a>
            </div>
            @endforeach
        </div>
    </div>
