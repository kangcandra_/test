<?php

use Illuminate\Database\Seeder;

class Kontak extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create(); //import library faker
        $limit = 20; //batasan berapa banyak data
        for ($i = 0; $i < $limit; $i++) {
            DB::table('products')->insert([ //mengisi datadi database
                'name' => $faker->name,
                'price' => $faker->unique()->randomDigit, //email unique sehingga tidak ada yang sama
                'description' => $faker->phoneNumber
            ]);
        }
    }
}
