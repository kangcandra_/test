<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use Vinkla\Instagram\Instagram;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        schema::defaultStringLength(191);
        view()->composer('frontend.blog.side', function ($view) {
            $kategori = \App\Kategori::all();
            // $category = \App\Category::all();
            $tag = \App\Tag::all();
            $recent = \App\Artikel::orderBy('created_at', 'desc')->take(4)->get();
            $view->with(compact('kategori', 'recent', 'tag'));
        });
        view()->composer('part.frontend.footer', function ($view) {
            $instagram = new Instagram('1722842458.1677ed0.4a12518cab3f446eaf28cb917d8fed41');
            // $instagram->get();
            $a = $instagram->media();
            $view->with(compact('a'));
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
