<?php

namespace App\Http\Controllers;

use App\Artikel;
use Illuminate\Http\Request;
use App\Kategori;
use File;
use Auth;
use App\Role;
use Alert;
use App\Tag;

class ArtikelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        if ($user->hasRole('admin')) {
            $artikel = Artikel::all();
        } elseif ($user->hasRole('member')) {
            $artikel = Auth::user()->Artikel;
        }
        // dd($artikel);
        return view('admin.artikel.index', compact('artikel'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $kategori = Kategori::all();
        $tag = Tag::all();
        return view('admin.artikel.create', compact('kategori', 'tag'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = Auth::user()->id;
        $artikel = new Artikel;
        $artikel->judul = $request->judul;
        $artikel->konten = $request->konten;
        $artikel->slug =str_slug($request->judul, '-') . '-' . str_random(6);
        $artikel->kategori_id = $request->kategori_id;
        $artikel->user_id = $user;
        $artikel->foto = $request->foto;
        // $artikel->publish = $request->publish;
        // upload foto
        // if ($request->hasFile('foto')) {
        //     $file = $request->file('foto');
        //     $destinationPath = public_path().'/assets/img/fotoartikel/';
        //     $filename = str_random(6).'_'.$file->getClientOriginalName();
        //     $uploadSuccess = $file->move($destinationPath, $filename);
        //     $artikel->foto = $filename;
        // }
        $artikel->save();
        $artikel->Tag()->attach($request->tag);
        Alert::success('Data successfully saved', 'Good Job')->autoclose(1000);
        return redirect()->route('admin.artikel.index');
    }

    public function Publish($id)
    {
        $artikel = Artikel::find($id);

        if ($artikel->status === 1) {
            $artikel->status = 0;
            Alert::success('Data successfully unpublished', 'Good Job')->autoclose(1000);
        } else {
            $artikel->status= 1;
            Alert::success('Data successfully published', 'Good Job')->autoclose(1000);
        }

        $artikel->save();
        return redirect()->route('admin.artikel.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\artikel  $artikel
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $artikel = Artikel::findOrFail($id);
        return view('admin.artikel.show', compact('artikel'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\artikel  $artikel
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $artikel = Artikel::findOrFail($id);
        $kategori = Kategori::all();
        $selectedCategory = Artikel::findOrFail($id)->categories_id;
        $selected = $artikel->Tag->pluck('id')->toArray();
        $tag = Tag::all();
        // dd($selected);
        return view('admin.artikel.edit', compact('artikel', 'kategori', 'selected', 'tag', 'selectedCategory'));

        // $selected = Artikel::findOrFail($artikel->id)->kategori_id;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\artikel  $artikel
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $artikel = Artikel::findOrFail($id);
        $artikel->judul = $request->judul;
        $artikel->konten = $request->konten;
        $artikel->slug =str_slug($request->judul, '-').'-'.str_random(6);
        $artikel->kategori_id = $request->kategori_id;
        $artikel->foto = $request->foto;
        // $artikel->publish = $request->publish;

        // edit upload foto
        // if ($request->hasFile('foto')) {
        //     $file = $request->file('foto');
        //     $destinationPath = public_path().'/assets/img/fotoartikel/';
        //     $filename = str_random(6).'_'.$file->getClientOriginalName();
        //     $uploadSuccess = $file->move($destinationPath, $filename);

        //     // hapus foto lama, jika ada
        //     if ($artikel->foto) {
        //         $old_foto = $artikel->foto;
        //         $filepath = public_path() . DIRECTORY_SEPARATOR . '/assets/img/fotoartikel'
        // . DIRECTORY_SEPARATOR . $artikel->foto;
        //         try {
        //             File::delete($filepath);
        //         } catch (FileNotFoundException $e) {
        //             // File sudah dihapus/tidak ada
        //         }
        //     }
        //     $artikel->foto = $filename;
        // }

        $artikel->save();
        $artikel->Tag()->sync($request->tag);
        Alert::success('Data successfully edited', 'Good Job')->autoclose(1000);
        return redirect()->route('admin.artikel.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\artikel  $artikel
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $artikel = Artikel::findOrFail($id);
        if ($artikel->foto) {
            $old_foto = $artikel->foto;
            $filepath = public_path() . DIRECTORY_SEPARATOR . 'assets/img/fotoartikel/'
            . DIRECTORY_SEPARATOR . $artikel->foto;
            try {
                File::delete($filepath);
            } catch (FileNotFoundException $e) {
                // File sudah dihapus/tidak ada
            }
        }
        $artikel->delete();
        Alert::success('Data successfully deleted', 'Good Job')->autoclose(1250);
        return redirect()->route('admin.artikel.index');
    }
}
